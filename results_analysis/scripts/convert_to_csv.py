import os
import pickle
import pandas as pd

from glob import glob

def to_csv():
    pickles = glob("/home/liadd/research/notebook/pickles/full/merged_4*.pickle")
    csv_dir = "/home/liadd/research/notebook/pickles/full/csv"

    for p in pickles:
        with open(p, "rb") as pc:
            df = pickle.load(pc)
            file_name = os.path.basename(p)
            file_name = file_name.split(".")[0]
            csv_path = os.path.join(csv_dir, file_name+".csv")
            df.to_csv(os.path.join(csv_dir, file_name+".csv"))
            print(f"{p}->{csv_path}")

def from_csv():
    pickles = glob("/home/liad/dev/research/notebooks/pickles/full/csv/merged_4*.csv")
    pickles_full_dir = "/home/liad/dev/research/notebooks/pickles/full/"
    
    for p in pickles:
        df = pd.read_csv(p)
        df = df.set_index("Unnamed: 0")
        df_name = os.path.basename(p).split(".")[0]
        new_df_name = df_name + ".pickle"
        new_df_path = os.path.join(pickles_full_dir, new_df_name)
        print(f"{p}->{new_df_path}")
        with open(new_df_path, "wb") as f:
            pickle.dump(df, f)

if __name__ == "__main__":
    from_csv()