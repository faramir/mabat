#!/usr/bin/env python
# coding: utf-8

# # Aggregated Measures Figures

# In[3]:


figures_dir = "/home/liadd/research/figures"

def save_fig(fig, name, end="jpg"):
    full_path = f"{figures_dir}/{name}.{end}"
    fig.savefig(full_path)


# In[142]:


import numpy as np
import pandas as pd

from glob import glob
from enum import Enum
from collections import OrderedDict

FREQUENCY = 10
COLUMNS = [0,0.01,0.02,0.04,0.08,0.16,0.32,0.64]
ROWS = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]

num_iocs_df = pd.read_csv("/home/liadd/research/notebook/data-vt-v22/attack_num_iocs_vt_v22.csv")

class ConfusionMatrix(Enum):
    RECALL = 0
    PRECISION = 1
    AVG_REWARD = 2

def get_attack_num_iocs(attack_id):
    df = num_iocs_df[num_iocs_df["attack"] == int(attack_id)]
    return int(df["num_iocs"])

def get_attacks_with_num_iocs(niocs_lower_bound=0, niocs_upper_bound=50000):
    df = num_iocs_df[num_iocs_df["num_iocs"] >= niocs_lower_bound]
    df = num_iocs_df[num_iocs_df["num_iocs"] <= niocs_upper_bound]
    return list(df['attack'])

def get_attacks_in_dir(results_dir):
    attack_ids = set()
    for attack_file in glob(f"{results_dir}/cm_*_*.csv*"):
        attack_id = attack_file.split("_")[-3]
        attack_ids.add(attack_id)
    return attack_ids
    
def get_attack_over_time(results_dir, attack_id, iterations, measure, rfp=None, rfn=None, avg_it=2):
    i = 0
    dfs = {}
    attack_num_iocs = -1
    
    # create data frame for each iteration
    while (i <= iterations):
        if rfp == None and rfp == None:
            dfs[i] = pd.DataFrame(columns=COLUMNS, index=ROWS)
        i += FREQUENCY
    
    for attack_file in glob(f"{results_dir}/cm_*_{attack_id}_*_*.csv"):
        attack_file_df = pd.read_csv(attack_file)
        # each attack is a single configuration, i.e. single fp and fn values.
        for r in attack_file_df.iterrows():
            it = r[1]['iteration']
            fp = r[1]['ex_fp']
            fn = r[1]['ex_fn']
            cm_tp, cm_tn, cm_fp, cm_fn = r[1]['cm_tp'],r[1]['cm_tn'],r[1]['cm_fp'],r[1]['cm_fn']
            
            if (it > iterations):
                # sometimes we will ask for less iterations then available
                # thus no need to keep going
                break
                
            if measure == ConfusionMatrix.AVG_REWARD:
                mes = (cm_tp + cm_fp) / float(it)
            elif measure == ConfusionMatrix.RECALL:
                if attack_num_iocs < 0:
                    attack_num_iocs = get_attack_num_iocs(attack_id)
                mes = cm_tp / float(attack_num_iocs)
            elif measure == ConfusionMatrix.PRECISION:
                mes = cm_tp / float(cm_tp + cm_fp)
            
            # set measure in corresponding df and corresponding fp and fn
            if rfp == None and rfn == None:
                try:
                    # already there is a value
                    if not pd.isna(dfs[it][fp][fn]):
                        if type(dfs[it][fp][fn]) is list:
                            dfs[it][fp][fn].append(mes)
                            if len(dfs[it][fp][fn]) == avg_it:
                                # put mean instead of accumulated values
                                dfs[it][fp][fn] = np.array(dfs[it][fp][fn]).mean()
                        else:
                            # create a new list of values
                            dfs[it][fp][fn] = [dfs[it][fp][fn], mes]
                            if len(dfs[it][fp][fn]) == avg_it:
                                # put mean instead of accumulated values
                                dfs[it][fp][fn] = np.array(dfs[it][fp][fn]).mean()
                    else:
                        dfs[it][fp][fn] = mes
                except:
                    pass
            elif (rfp and rfn == None):
                if rfp == fp:
                    pass
            elif (rfp == None and rfn):
                if rfn == fn:
                    pass
            # measure requested in specific fn/fp
            elif (fp == rfp and fn == rfn):
                dfs[it] = mes
    return dfs

def get_attacks_over_time(results_dir, iterations, measure, avg=False, rfp=None, rfn=None):
    attacks_in_dir = get_attacks_in_dir(results_dir)
    attack_results = {}
    
    for attack in attacks_in_dir:
        print(".", end=" ")
        attack_results[attack] = get_attack_over_time(results_dir, attack_id=attack, iterations=iterations,
                                                     measure=measure, rfp=rfp, rfn=rfn)
    temp = []
    if avg:
        if rfp == None and rfn == None:
            for k,v in attack_results.items():
                v_array = list(v.values())
                v_array = np.array([d.to_numpy() for d in v_array])
                
                if len(temp) == 0:
                    temp = v_array
                else:
                    temp += v_array
            return dict(zip(v.keys(), temp / len(attacks_in_dir)))
        else:
            for k,v in attack_results.items():
                v_array = np.array(list(v.values()))
                if len(temp) == 0:
                    temp = v_array
                else:
                    temp += v_array
            return dict(zip(v.keys(), temp / len(attacks_in_dir)))
    else:
        return attack_results
        


# In[161]:


import pandas as pd
import numpy as np
import pylab as pl
import matplotlib.pyplot as plt
import seaborn as sns

plt.style.use('seaborn-whitegrid')

def get_average_measure_all(results_base_num, results_dir, measure, iterations, rfp=0, rfn=0):
    ucb1rnddf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 7}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    rnddf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 3}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    saucbmsdf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 4}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    saeg01msdf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    ucb1msdf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 6}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    rndmsdf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 9}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    eg01rnddf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 2}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    saucbrnddf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 5}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    msdf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 8}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    eg01msdf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 1}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    oracledf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 10}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    print("")
    
    ret_tuple = (ucb1rnddf, rnddf, saucbmsdf, saeg01msdf, 
                 ucb1msdf, rndmsdf, eg01rnddf, saucbrnddf, 
                 msdf, eg01msdf, oracledf)

    return ret_tuple


def plot_aggregated(results_tuple, xlim=15000, measure="", title="", fp=0, fn=0, yticks=None, fig_path="", legend=True):
    (ucb1rnddf, rnddf, saucbmsdf, saeg01msdf, 
     ucb1msdf, rndmsdf, eg01rnddf, saucbrnddf, 
     msdf, eg01msdf, oracledf) = results_tuple
    
    
    fig, ax = plt.subplots(figsize=(14, 14))    
#     fig.set_xlim(0,xlim)
    
    sns.lineplot(list(oracledf.keys()),
                 list(oracledf.values()),
                 linewidth=5, label="ORACLE", color="darkred")

    sns.lineplot(list(saeg01msdf.keys()),
                 list(saeg01msdf.values()),
                 linewidth=5, label="SAEG01-MS", color="tab:blue")

    sns.lineplot(list(saucbmsdf.keys()),
             list(saucbmsdf.values()),
             linewidth=5, label="SAUCB-MS", color="tab:green")

    sns.lineplot(list(eg01msdf.keys()),
             list(eg01msdf.values()),
             linewidth=5, label="EG01-MS", color="tab:orange")

    sns.lineplot(list(ucb1msdf.keys()),
             list(ucb1msdf.values()),
             linewidth=5, label="UCB1-MS", color="tab:red")

    sns.lineplot(list(saucbrnddf.keys()),
             list(saucbrnddf.values()),
             linewidth=5, label="SAUCB-Rnd", color="tab:purple")

    sns.lineplot(list(ucb1rnddf.keys()),
             list(ucb1rnddf.values()),
             linewidth=5, label="UCB1-Rnd", color="tab:pink")

    sns.lineplot(list(eg01rnddf.keys()),
             list(eg01rnddf.values()),
             linewidth=5, label="EG01-Rnd", color="tab:brown")

    sns.lineplot(list(msdf.keys()),
             list(msdf.values()),
             linewidth=5, label="MS", color="tab:olive")

    sns.lineplot(list(rndmsdf.keys()),
             list(rndmsdf.values()),
             linewidth=5, label="Rnd-MS", color="tab:gray")

    sns.lineplot(list(rnddf.keys()),
             list(rnddf.values()),
             linewidth=5, label="Rnd-Rnd",color="tab:cyan")
        
#     fig.suptitle(title, fontweight="bold", fontsize=24)
    plt.xlabel('Iteration', fontsize=50)
    plt.ylabel(measure, fontsize=50)
#     plt.legend(loc="upper right", ncol=3, prop={'size': 24})
    if legend:
        plt.legend(loc='upper right', ncol=1,  prop={'size': 35})

    ax.set_xscale('log')
    plt.xticks(fontsize=50)
    plt.yticks([0,0.2,0.4,0.6,0.8,1.0], fontsize=50)
    plt.tight_layout()
    save_fig(fig, fig_path)
                 


###############
#### WITH #####
###############

with_base_num = 587327
with_results_dir="/home/liadd/research/results/2021_6_4"
iterations=15000
# with_average_reward = get_average_measure_all(with_base_num, with_results_dir, 
#                                               measure=ConfusionMatrix.AVG_REWARD, iterations=iterations)
# plot_aggregated(with_average_reward, measure="Average Reward", 
#                 title="Average Reward (With actual attack, no noise)",
#                 fig_path="aggregated-measures/average-reward-with-attack-no-noise", legend=True)
# # with_precision = get_average_measure_all(with_base_num, with_results_dir, 
# #                                               measure=ConfusionMatrix.PRECISION, iterations=iterations)
# # plot_aggregated(with_precision, measure="Precision", title="Precision (With actual attack, no noise)",
# #                 fig_path="aggregated-measures/precision-with-attack-no-noise")
with_recall = get_average_measure_all(with_base_num, with_results_dir, 
                                      measure=ConfusionMatrix.RECALL, iterations=iterations)
plot_aggregated(with_recall, 0, measure="Recall", title="Recall (With actual attack, no noise)",
                fig_path="aggregated-measures/recall-with-attack-no-noise", legend=False)


###################
##### WITHOUT #####
###################

without_base_num = 587316
without_results_dir="/home/liadd/research/results/2021_6_4"
iterations=15000
# without_average_reward = get_average_measure_all(without_base_num, without_results_dir, 
#                                               measure=ConfusionMatrix.AVG_REWARD, iterations=iterations)
# plot_aggregated(without_average_reward, measure="Average Reward", 
#                 title="Average Reward (Without actual attack, no noise)",
#                 fig_path="aggregated-measures/average-reward-without-attack-no-noise",legend=True)
# without_precision = get_average_measure_all(without_base_num, without_results_dir, 
#                                               measure=ConfusionMatrix.PRECISION, iterations=iterations)
# plot_aggregated(without_precision, measure="Precision", title="Precision (without actual attack, no noise)",
#                 fig_path="aggregated-measures/precision-without-attack-no-noise")

without_recall = get_average_measure_all(without_base_num, without_results_dir, 
                                      measure=ConfusionMatrix.RECALL, iterations=iterations)
plot_aggregated(without_recall, 0, measure="Recall", title="Recall (without actual attack, no noise)",
                fig_path="aggregated-measures/recall-without-attack-no-noise", legend=False)

