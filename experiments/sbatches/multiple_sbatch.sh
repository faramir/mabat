#!/bin/bash
BASE_DIR=/home/liad/dev/mabat
SBATCH=$BASE_DIR/experiments/sbatches/my_sbatch.sh

########
# with #
########
# $SBATCH -n 20 -c fp-fn-egreedy-mmshared-with.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-egreedy-mshared-with.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-egreedy-random-with.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-random-with.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-ucb1-mmshared-with.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-ucb1-mrandom-with.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-ucb1-mshared-with.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-ucb1-random-with.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-ms-with.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-random-ms-with.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-oracle-with.json -p aggregated-measures

###########
# without #
###########
$SBATCH -n 20 -c fp-fn-egreedy-mmshared-without.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-egreedy-mshared-without.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-egreedy-random-without.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-random-without.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-ucb1-mmshared-without.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-ucb1-mrandom-without.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-ucb1-mshared-without.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-ucb1-random-without.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-ms-without.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-random-ms-without.json -p aggregated-measures
# $SBATCH -n 20 -c fp-fn-oracle-without.json -p aggregated-measures

###########
#   Rank  #
###########
# $SBATCH -n 2 -c rank-with.json -p evading-attack