export BASE_DIR=$(realpath ./)

##############################
#### user can edit these #####
##############################
export MABAT_CONDA_ENV_NAME=mabat
export RESULTS_DIR_PATH=$BASE_DIR/experiments/results
export FIGURES_DIR_PATH=$BASE_DIR/results_analysis/figures
export PICKLES_DIR_PATH=/home/liadd/research/notebook/pickles
export ATTACK_DB_DATA_DIR_PATH=$BASE_DIR/results_analysis/data-vt-v22
##############################

SBATCH_TEMPLATE_FILE=${BASE_DIR}/experiments/sbatches/sbatch.template
MULTIPLE_SBATCH_FILE=${BASE_DIR}/experiments/sbatches/multiple_sbatch.sh
RUNNER=${BASE_DIR}/src/runner.py

find_in_conda_env(){
    conda env list | grep "${@}" >/dev/null 2>/dev/null
}

if find_in_conda_env "${MABAT_CONDA_ENV_NAME}*"; then
    echo ""
    echo "Using '$MABAT_CONDA_ENV_NAME' conda env."

    # update BASE_DIR name
    sed -i "s|BASE_DIR=.*|BASE_DIR=${BASE_DIR}|" ${SBATCH_TEMPLATE_FILE}
    sed -i "s|BASE_DIR=.*|BASE_DIR=${BASE_DIR}|" ${MULTIPLE_SBATCH_FILE}
    # update RESULTS_DIR_PATH
    sed -i "s|RESULTS_DIR_PATH=.*|RESULTS_DIR_PATH=${RESULTS_DIR_PATH}|" ${SBATCH_TEMPLATE_FILE}
    # update CONDA_ENV_NAME name
    sed -i "s|MABAT_CONDA_ENV_NAME=.*|MABAT_CONDA_ENV_NAME=${MABAT_CONDA_ENV_NAME}|" ${SBATCH_TEMPLATE_FILE}

    alias runner='python ${RUNNER}'
    alias mabat_multiple_sbatch='./experiments/sbatches/multiple_sbatch.sh'
    alias mabat_sbatch='./experiments/sbatches/my_sbatch.sh'
    alias mabatenv='conda activate ${MABAT_CONDA_ENV_NAME}'

    echo ""
    echo "Available Utils:"
    echo " [*] runner - run experiments directly - no sbatch"
    echo " [*] mabat_sbatch - run single sbatch"
    echo " [*] mabat_multiple_sbatch - run multiple sbatches"
    echo " [*] mabatenv - activate mabat conda env"
    echo ""
else 
    echo "Conda env '$MABAT_CONDA_ENV_NAME' doesnt exists."
    read -p "Create conda env '$MABAT_CONDA_ENV_NAME'? [Yy/Nn]:" -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]]; then
        echo "Exiting."
        exit 1
    else
        conda env create -n ${MABAT_CONDA_ENV_NAME} -f ${BASE_DIR}/environment.yml
        echo "Please source activate.sh again."
    fi
fi


