import random
import itertools

from math import sqrt
from IPython import embed
from .abstract_order import Order


class RandomMostSharedOrder(Order):
    """
    On every iteration, search for the most shared observable
    """

    def __init__(self, host_env, logger, order_params):
        super(RandomMostSharedOrder, self).__init__(
            host_env, logger, order_params)

    def order(self):
        """
        Generator to determine the order of iteration over objects.
        We say object because it can be only IOCs objects, or attacks pointing
        to IOCs list.
        """

        while True:
            random_attack_id = random.choice(
                list(self.host_env.elements.keys()))

            # select machine to pull by the number of sharing attacks.
            ioc = self.host_env.get_random_max_shared_ioc(
                random_attack_id)

            # in case select an attack that run out of iocs
            if not ioc:
                continue

            self.host_env.remove_ioc_from_attack(
                ioc.id, random_attack_id)
            yield ioc
