import random

from algorithms.ucb1 import UCB1
from .abstract_order import Order


class UCB1MultiRandomOrder(Order):
    """
    Order based on UCB algorithm, with multiple armes pulling.

    Multi Armes ?
    -----------
        There are attack sharing IOCs. In case we choose an IOC of some attack
        there is no reason to recheck again when pulling other attack.
        Here for every IOC searching (pulling a machine arm) we will pull
        all attacks which has this IOC as well. Their reward value will be
        updated as well.
    """

    def __init__(self, host_env, logger, order_params):
        super(UCB1MultiRandomOrder, self).__init__(
            host_env, logger, order_params)

    def order(self):
        """
        Generator to determine the order of iteration over objects.
        We say object because it can be only IOCs objects, or attacks pointing
        to IOCs list.
        """
        # number of bandits is as number of attacks
        self.ucb = UCB1(len(self.host_env.elements), self.logger)
        self.items = list(self.host_env.elements.items())
        # shuffle attacks order inplace
        random.shuffle(self.items)

        while True:
            selected_attack_index = self.ucb.select_arm()
            selected_attack_id, _ = self.items[selected_attack_index]
            # get all attack iocs that are *not* removed
            attack_iocs = self.host_env.get_iocs_left(selected_attack_id)

            if not attack_iocs:
                # we checked all iocs of attack
                # hence we put -inf in so it won't be selected anymore
                self.ucb.values[selected_attack_index] = -1e400
                if selected_attack_id == self.host_env.occuring_attack_id:
                    self.stop_condition = True
                continue

            # select random ioc to check
            ioc = random.choice(attack_iocs)
            # compute reward for selected attack
            reward = 1 if ioc.occurs else 0

            # remove from attack from env and pull *multiple* machines
            attacks_removed_from = self.host_env.remove_ioc_from_env(ioc.id)
            for attack_id in attacks_removed_from:
                attack_index = get_attack_index(attack_id, self.items)
                self.ucb.update(attack_index, reward)
            yield ioc

    def run(self, attack_id, param, run_params):
        norm_cost, _ = super(UCB1MultiRandomOrder, self).run(
            attack_id, param, run_params)
        # we are adding another check, specific for UCB algorithms.
        max_ucb_machine_index = self.ucb.get_random_max_ranked_machine()
        occuring_attack_index = get_attack_index(
            self.host_env.occuring_attack_id, self.items)
        result = (occuring_attack_index == max_ucb_machine_index)

        self.logger.debug("attack success result: {} occuring_attack={}, max_ranked_attack={}".format(
            int(result), occuring_attack_index, max_ucb_machine_index))
        return norm_cost, result


def get_attack_index(requested_attack_id, lst):
    for idx, attack in enumerate(lst):
        attack_id, _ = attack
        if attack_id == requested_attack_id:
            return idx
