import ipdb
import random
import numpy as np

from .abstract_order import Order
from algorithms.thompson_sampling import ThompsonSampling


class ThompsonSamplingMultiMostSharedOrder(Order):
    """
    Order based on Thompson Sampling algorithm.
    """

    def __init__(self, host_env, logger, order_params):
        super(ThompsonSamplingMultiMostSharedOrder, self).__init__(
            host_env, logger, order_params)

    def order(self):
        """
        Generator to determine the order of iteration over objects.
        We say object because it can be only IOCs objects, or attacks pointing
        to IOCs list.
        """
        # number of bandits is as number of attacks
        self.thompson = ThompsonSampling(
            len(self.host_env.elements), {})
        self.items = list(self.host_env.elements.items())
        # shuffle attacks order inplace
        random.shuffle(self.items)

        while True:
            selected_attack_index = self.thompson.get_arm(None)
            selected_attack_id, _ = self.items[selected_attack_index]
            # get all attack iocs that are *not* removed
            attack_iocs = self.host_env.get_iocs_left(selected_attack_id)

            if not attack_iocs:
                # we checked all iocs of attack
                # by putting *np.inf* in number of pulls, the beta
                # distribution of this arm will always return 0
                # thus the arm will not be selected anymore.
                self.thompson.pulls[selected_attack_index] = np.inf
                if selected_attack_id == self.host_env.occuring_attack_id:
                    if self.run_params['early_occuring_attack_stop']:
                        self.stop_condition = True
                continue

            # select machine to pull by the number of sharing attacks.
            ioc = self.host_env.get_random_max_shared_ioc(selected_attack_id)
            # calculate reward for chosen ioc
            reward = 1 if ioc.occurs else 0

            # remove from attack from env and pull *multiple* machines
            attacks_removed_from = self.host_env.remove_ioc_from_env(ioc.id)
            for attack_id in attacks_removed_from:
                attack_index = get_attack_index(attack_id, self.items)
                self.thompson.update(attack_index, reward)

            yield ioc


def get_attack_index(requested_attack_id, lst):
    for idx, attack in enumerate(lst):
        attack_id, _ = attack
        if attack_id == requested_attack_id:
            return idx
