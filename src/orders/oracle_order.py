import random

from IPython import embed
from .abstract_order import Order


class OracleOrder(Order):
    """
    Oracle policy order.
    """

    def __init__(self, host_env, logger, order_params):
        super(OracleOrder, self).__init__(
            host_env, logger, order_params)

    def order(self):
        """
        Generator to determine the order of iteration over objects.
        We say object because it can be only IOCs objects, or attacks pointing
        to IOCs list.

        Selects random artifact associated with the ongoing attack.
        The selected artifact exists in the knowledge base. This is important
        in case the ongoing attack is removed.
        """
        # We are always pulling the occuring attack.
        available_iocs = self.host_env.get_iocs_exits_and_left(
            self.host_env.occuring_attack_id)

        while True:
            # optimal attack artifact left
            if available_iocs:
                random_attack_ioc = available_iocs.pop(
                    random.randrange(len(available_iocs)))
            # just random policy
            else:
                if self.run_params['early_occuring_attack_stop']:
                    self.stop_condition = True

                random_attack_id = random.choice(
                    list(self.host_env.elements.keys()))
                random_attack_ioc = random.choice(
                    list(self.host_env.elements[random_attack_id]))

                if random_attack_ioc.removed:
                    # just randomize another one
                    continue

            # let's remove from env and yield.
            self.host_env.remove_ioc_from_attack(
                random_attack_ioc.id, self.host_env.occuring_attack_id)
            yield random_attack_ioc
