import random

from .abstract_order import Order


class MostSharedOrder(Order):
    """
    Iterate over all iocs in the graph in random order.
    """

    def __init__(self, host_env, logger, order_params):
        super(MostSharedOrder, self).__init__(
            host_env, logger, order_params)

    def get_ioc_from_attack(self, ioc_id, attack_id):
        for ioc in self.host_env.elements[attack_id]:
            if ioc.id == ioc_id:
                return ioc

    def order(self):
        """
        Generator to determine the order of iteration over objects.
        We say object because it can be only IOCs objects, or attacks pointing
        to IOCs list.
        """
        shared_iocs_copy = self.host_env._shared_iocs.copy()
        iocs_by_legnth = sort_dict_by_value_length(shared_iocs_copy)
        iocs_by_legnth_lst = list(zip(
            iocs_by_legnth.keys(), iocs_by_legnth.values()))

        while True:
            ioc_id, ioc_sharing_attacks = iocs_by_legnth_lst.pop(0)
            random_attack_id = random.choice(ioc_sharing_attacks)

            # since there are no UCB machines here, keep pulling different machines
            # for the same ioc is a waste of our running time.
            ioc = self.get_ioc_from_attack(ioc_id, random_attack_id)
            self.host_env.remove_ioc_from_env(ioc_id)

            yield ioc


def sort_dict_by_value_length(d):
    return {k: v for k, v in sorted(
            d.items(), key=lambda item: len(item[1]), reverse=True)}
