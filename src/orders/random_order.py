import random
import itertools

from math import sqrt
from IPython import embed
from .abstract_order import Order


class RandomOrder(Order):
    """
    Iterate over all iocs in the graph in random order.
    """

    def __init__(self, host_env, logger, order_params):
        super(RandomOrder, self).__init__(
            host_env, logger, order_params)

    def order(self):
        """
        Generator to determine the order of iteration over objects.
        We say object because it can be only IOCs objects, or attacks pointing
        to IOCs list.
        """

        while True:
            random_attack_id = random.choice(
                list(self.host_env.elements.keys()))
            random_attack_ioc = random.choice(
                list(self.host_env.elements[random_attack_id]))

            if random_attack_ioc.removed:
                # just randomize another one
                continue
            else:
                # let's remove from env and yield
                self.host_env.remove_ioc_from_attack(
                    random_attack_ioc.id, random_attack_id)
                yield random_attack_ioc
