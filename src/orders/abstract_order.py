import numpy as np
import pandas as pd

from IPython import embed
from runner_utils import save_results


class Order(object):
    """
    Abstrat class to Order object.
    All order objects should inherit form this class and implement
    the order method which will set the order of investigation.
    """

    def __init__(self, host_env, logger, order_params):
        self.host_env = host_env
        self.logger = logger
        self.order_params = order_params
        self.run_params = None
        self.stop_condition = False

    def order(self):
        """
        Generator to determine the order of iteration over objects.
        We say object because it can be only IOCs objects, or attacks pointing
        to IOCs list.
        """
        raise NotImplementedError

    def get_avg_num_iocs(self, run_params):
        # there are tests in which the occuring attack is removed
        attacks = set(dict(run_params['attacks']).keys()).intersection(
            set(self.host_env.elements.keys()))
        attacks_num_iocs = [len(self.host_env.elements[a]) for a in attacks]
        avg_number_of_iocs = sum(attacks_num_iocs) / \
            float(len(attacks_num_iocs))
        return avg_number_of_iocs

    def run(self, attack_id, param, run_params):
        # let the order object dictate the order by create a generator.
        self.run_params = run_params
        run_type = run_params["run_type"]
        run_current_fn = run_params["current_fn"]

        total_number_of_iocs = len(self.host_env.occuring_attack_iocs)
        attack_iocs_by_id = [i.id for i in self.host_env.occuring_attack_iocs]
        cost, found_until_now = 0, 0
        self.true_positive_num, self.false_positive_num = 0, 0
        self.true_negative_num, self.false_negative_num = 0, 0

        for ioc in self.order():
            # if we already have the search result, no need to search
            # thus no need to pay cost. In case ioc not in db, will add it
            if self.host_env._get_search_result(ioc):
                pass
            else:
                self.rank_check(cost)
                self.budget_check(cost)
                self.cmatrix_check(cost)
                cost += 1

                if ioc.occurs and ioc.id in attack_iocs_by_id:
                    self.true_positive_num += 1
                elif ioc.occurs and ioc.id not in attack_iocs_by_id:
                    self.false_positive_num += 1
                elif not ioc.occurs and ioc.id not in attack_iocs_by_id:
                    self.true_negative_num += 1
                elif not ioc.occurs and ioc.id in attack_iocs_by_id:
                    self.false_negative_num += 1

                found_until_now += 1 if ioc.occurs else 0

            if run_type == "thresholds":
                num_iocs_discoverd = self.host_env.get_num_iocs_discoverd(
                    attack_id)
                current_thresh = num_iocs_discoverd / \
                    float(total_number_of_iocs)
                succ_rate = found_until_now / float(cost)
                num_occuring_left = self.host_env.get_num_occuring_iocs_left(
                    attack_id)
                self.logger.info(f'num_occuring_left={num_occuring_left}')

                if current_thresh >= param or \
                        (self.run_params['early_occuring_attack_stop'] and
                            (num_occuring_left == 0 or self.stop_condition)):

                    self.logger.debug(
                        "attack_id={}, cost={}".format(attack_id, cost))
                    self.rank_save_results()
                    self.budget_save_results()
                    self.cmatrix_save_results()

                    if num_occuring_left == 0 or self.stop_condition:
                        self.logger.info("Early stopping!")

                    return succ_rate, None

            elif run_type == "budgets":
                num_iocs_discoverd = self.host_env.get_num_iocs_discoverd(
                    attack_id)
                current_thresh = num_iocs_discoverd / \
                    float(total_number_of_iocs)
                if cost >= param or \
                        (self.run_params['early_occuring_attack_stop'] and
                            (current_thresh >= 1-run_current_fn or
                                self.stop_condition)):
                    self.logger.debug(
                        "attack_id={}, cost={}".format(
                            attack_id, cost))
                    forenzic_data_prec = num_iocs_discoverd / \
                        float(total_number_of_iocs)

                    self.rank_save_results()
                    self.budget_save_results()
                    self.cmatrix_save_results()
                    return forenzic_data_prec, None

            elif run_type == "rewards":
                succ_rate = found_until_now / float(cost)
                if found_until_now >= total_number_of_iocs:
                    self.logger.debug(
                        "attack_id={}, cost={}, reward={}".format(
                            attack_id, cost, found_until_now))
                    # return found_until_now, None
                    return succ_rate, None
                elif cost >= param:
                    self.logger.debug(
                        "attack_id={}, cost={}, reward={}".format(
                            attack_id, cost, found_until_now))
                    # return found_until_now, None
                    return succ_rate, None

        self.logger.warn(
            f"Finished without reaching end condition ({run_type})")

        # we normalize the total cost of each attack since attacks
        # have different number of iocs. Obviously attack with bigger number
        # of iocs should take longer to be found.
        return cost, None

    def rank_check(self, T):
        if self.run_params['create_rank_results'] and \
                not self.host_env._remove_occuring_attack:
            # create df if not exists
            if not hasattr(self, "place_df"):
                self.place_df = pd.DataFrame(
                    columns=[self.__class__.__name__, "iteration"])

            algo_obj = self.ucb if 'UCB' in self.__class__.__name__ else self.egreedy

            place_for_index = algo_obj.get_place_for_index()
            occuring_attack_index = get_attack_index(
                self.host_env.occuring_attack_id, self.items)

            occuring_attack_place = place_for_index[occuring_attack_index]
            # append to attacks df
            r = pd.DataFrame.from_dict(
                {self.__class__.__name__: [occuring_attack_place],
                 "iteration": [T]})
            self.place_df = self.place_df.append(r, ignore_index=True)

    def rank_save_results(self):
        if self.run_params['create_rank_results'] and \
                not self.host_env._remove_occuring_attack:
            save_results(
                {f"place/place_{self.__class__.__name__ }_{self.host_env.occuring_attack_id}": self.place_df})

    def budget_check(self, T):
        if self.run_params['create_budget_results'] and T % 3 == 0:
            total_number_of_iocs = len(self.host_env.occuring_attack_iocs)
            num_iocs_discoverd = self.host_env.get_num_iocs_discoverd(
                self.host_env.occuring_attack_id)
            current_thresh = num_iocs_discoverd / \
                float(total_number_of_iocs)
            # create df if not exists
            if not hasattr(self, "budget_df"):
                self.budget_df = pd.DataFrame(
                    columns=[self.__class__.__name__, "iteration", "num_iocs"])

            r = pd.DataFrame.from_dict(
                {self.__class__.__name__: [current_thresh],
                 "iteration": [T],
                 "num_iocs": [total_number_of_iocs],
                 "attack_id": [self.host_env.occuring_attack_id]})
            self.budget_df = self.budget_df.append(r, ignore_index=True)

    def budget_save_results(self):
        if self.run_params['create_budget_results']:
            max_iteration = int(self.budget_df.tail(1)['iteration'])
            max_iteration_col = [max_iteration] * len(self.budget_df)
            self.budget_df['max_iteration'] = max_iteration_col
            save_results(
                {f"budget/budget{self.__class__.__name__ }_{self.host_env.occuring_attack_id}": self.budget_df})

    def cmatrix_check(self, T):
        if self.run_params['create_fp_fn_results'] and T % 1 == 0:
            num_iocs_discoverd = self.host_env.get_num_iocs_discoverd(
                self.host_env.occuring_attack_id)

            if T:
                avg_rew = num_iocs_discoverd / float(T)
            else:
                avg_rew = 0

            # create df if not exists
            if not hasattr(self, "cmatrix_df"):
                self.cmatrix_df = pd.DataFrame(
                    columns=["iteration", "ex_fp", "ex_fn", "cm_tp", "cm_tn",
                             "cm_fp", "cm_fn", "avg_rew"])

            r = pd.DataFrame.from_dict(
                {"iteration": [T],
                 "ex_fp": [self.run_params["current_fp"]],
                 "ex_fn": [self.run_params["current_fn"]],
                 "cm_tp": [self.true_positive_num],
                 "cm_tn": [self.true_negative_num],
                 "cm_fp": [self.false_positive_num],
                 "cm_fn": [self.false_negative_num],
                 "avg_rew": [avg_rew]})

            self.cmatrix_df = self.cmatrix_df.append(r, ignore_index=True)

    def cmatrix_save_results(self):
        if self.run_params['create_fp_fn_results']:
            # max_iteration = int(self.budget_df.tail(1)['iteration'])
            # max_iteration_col = [max_iteration] * len(self.budget_df)
            # self.budget_df['max_iteration'] = max_iteration_col
            save_results(
                {f"fp-fn/cm_{self.__class__.__name__ }_{self.host_env.occuring_attack_id}": self.cmatrix_df})


def get_attack_index(requested_attack_id, lst):
    for idx, attack in enumerate(lst):
        attack_id, _ = attack
        if attack_id == requested_attack_id:
            return idx
