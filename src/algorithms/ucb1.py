import math
import numpy as np
from random import choice
from IPython import embed


class UCB1():

    def __init__(self, n_arms, logger=None):
        self._logger = logger
        # number of pulls count
        self.counts = np.zeros((n_arms,), dtype=int)
        # average reward value
        self.values = np.zeros((n_arms,))

    def select_arm(self):
        n_arms = len(self.counts)

        for arm in range(n_arms):
            if self.counts[arm] == 0:
                return arm

        total_counts_log = math.log(self.counts.sum())
        exploration_coeff = np.sqrt(2 * total_counts_log) / self.counts
        # average_reward + exploration
        ucb_coeff = self.values + exploration_coeff

        # there might be multilpe machines with max value.
        # hence we use random as tie braker.
        max_value = ucb_coeff[np.argmax(ucb_coeff)]
        # get all indices with max value (all machines with max value)
        max_indices = np.where(ucb_coeff == max_value)[0]
        # random tie braker between all max values
        return np.random.choice(max_indices)

    def update(self, chosen_arm, reward, count_step_size=1):
        self.counts[chosen_arm] = self.counts[chosen_arm] + count_step_size
        n = self.counts[chosen_arm]
        value = self.values[chosen_arm]
        new_value = ((n - 1) / float(n)) * value + \
            (1 / float(n)) * reward
        self.values[chosen_arm] = new_value
        return

    def get_random_max_ranked_machine(self):
        max_indice = np.argmax(self.values)
        # get all indices with max value (all machines with max value)
        max_indices = np.where(self.values == self.values[max_indice])[0]
        return choice(max_indices)

    def get_place_for_index(self):
        total_counts_log = math.log(self.counts.sum())
        exploration_coeff = np.sqrt(2 * total_counts_log) / self.counts
        # average_reward + exploration
        ucb_coeff = self.values + exploration_coeff
        ucb_coeff = list(enumerate(ucb_coeff))
        ucb_coeff.sort(key=lambda x: x[1])
        place_for_index = {}
        for idx, t in enumerate(ucb_coeff):
            # create map between the origin index
            # to the new index (place)
            place_for_index[t[0]] = idx
        return place_for_index

