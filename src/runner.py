#! /usr/bin/python

import os
import argparse
import numpy as np
import pandas as pd

from IPython import embed
from host_env import HostEnvironmentBuilder

from orders import *
from runner_utils import (create_logger,
                          save_results,
                          load_json,
                          print_nice,
                          RunnerUtils)


def main(args):
    # initializations
    logger = create_logger(args.verbose)
    if not os.getenv("MABAT_CONDA_ENV_NAME"):
        logger.info("Did you run 'source activate.sh' ? Exiting.")

    config_path = os.path.join(
        os.getenv("BASE_DIR"), "experiments", "configs", args.config)
    config = load_json(config_path, logger)
    credentials, run_params = config['credentials'], config['run_params']
    run_params['base_dir'] = args.base_dir

    host_env_builder = HostEnvironmentBuilder(credentials, logger)
    # creating nutral environment for all runs.
    nutral_host_env = host_env_builder.build_nutral_env(run_params)
    nutral_host_env._iocs_search_db = {}
    nutral_host_env.calculate_shared_iocs()

    runner_utils = RunnerUtils(nutral_host_env, host_env_builder, args, logger)

    # DataFrames to hold the results
    orders = config['orders']

    print_nice(logger, "Configuration", config)

    # run experiments for all algorithms in orders
    for order_name, order_params in orders.items():
        logger.info("*" * 40)
        logger.info("{}".format(order_name))
        logger.info("*" * 40)
        order_class = globals()[order_name]
        # order_cost_column, order_detection_column =
        runner_utils.run_order_parallely(
            order_class, order_params, config, args.j, args.debugrun)

    logger.info("Done :)")
    save_results(save_log=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--config', '-c', default="configs/config.json",
                        help='run configuration file path')
    parser.add_argument('--verbose', '-v', action='count', default=0)
    parser.add_argument('-j', metavar='N', type=int,
                        help='number of process to use', default=1)
    parser.add_argument('--debugrun', "-d", action='store_true',
                        help="Run without process, convinient for debug runs")
    parser.add_argument('--base-dir', "-b", default='./',
                        help="Run without process, convinient for debug runs")
    args = parser.parse_args()
    main(args)
