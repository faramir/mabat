import os
import json
import shutil
import logging

import pathlib
import numpy as np
import pandas as pd

from os import environ
from copy import deepcopy
from IPython import embed
from pprint import pformat
from itertools import product
from datetime import datetime
from multiprocessing import Process, Manager

RESULTS_DIR = os.getenv('RESULTS_DIR_PATH')


class RunnerUtils(object):

    def __init__(self, host_nutral_env, host_env_builder, args, logger):
        self.logger = logger
        self.nutral_env = host_nutral_env
        self.env_builder = host_env_builder
        self.args = args

    def run_order(self, order, order_params, run_params,
                  attacks, attacks_shared_dict={}, fp_fn_shared_dict={}):
        """ Run all order experiments with given params.

        Input:
        -------
            :param order (Type of Order class): order type to ran
             experiment with. An instance of the given order will be created
             each run.
            :param order_params: param to initate order instance with.
            :param run_params (dict): contains all the required params for this
            series of runs such as: number of iterations, thresholds etc.

        Output:
        -------
            numpy.array - contains average cost of the given order.
        """
        fp_vals = run_params["fp"]
        fn_vals = run_params["fn"]
        run_type = run_params["run_type"]
        normalize_results = run_params["normalize_results"]
        remove_occuring_attack = run_params["remove_occuring_attack"]
        params = run_params[run_type]

        iterations = run_params["attack_iterations"]
        create_fp_fn_results = run_params["create_fp_fn_results"]
        create_attacks_results = run_params["create_attacks_results"]
        nproc = run_params.get("nproc") if run_params.get("nproc") else 0

        for param in params:
            self.logger.info("-" * 40)
            self.logger.info(f"{run_type[:-1].capitalize()} {param}")
            self.logger.info("-" * 40)

            base_file_name = f"{order.__name__}_{nproc}_{run_type}{param}"
            attacks_df = pd.DataFrame(
                columns=["attack", "reward", "num_iocs"])

            for str_attack_id, attack_name in attacks:
                self.logger.info("-" * 40)
                self.logger.info(f"attack={str_attack_id}")
                self.logger.info("-" * 40)
                attack_id = int(str_attack_id)
                # attack_total_cost = 0

                df_fp_fn_cost = pd.DataFrame(columns=fp_vals, index=fn_vals)
                for fp, fn in product(fp_vals, fn_vals):
                    self.logger.info("-" * 40)
                    self.logger.info(f"fp={fp}, fn={fn}")
                    self.logger.info("-" * 40)
                    run_params["current_fp"] = fp
                    run_params["current_fn"] = fn
                    conf_total_cost = 0

                    # repeat the experiment in order to have average results
                    for i in range(iterations):
                        self.logger.debug("+" * 10)
                        self.logger.debug("Iteration {}".format(i))
                        self.logger.debug("+" * 10)

                        # in order to give each run "clean slate" we
                        # create it's environment from a nutral and clean one.
                        host_env = deepcopy(self.nutral_env)
                        self.env_builder.set_env(
                            host_env, attack_id, fp=fp, fn=fn,
                            remove_attack=remove_occuring_attack)
                        # run the instance of order (algorithm) on the instance
                        # of the host environment, until occuring attack is found.
                        cost, _ = order(
                            host_env, self.logger, order_params).run(
                                attack_id, param, run_params)
                        conf_total_cost += cost
                        # attack_total_cost += cost
                    conf_avg_cost = conf_total_cost / float(iterations)
                    self.logger.debug(
                        f"conf_avg_cost-(fp={fp}, fn={fn}, attack={attack_id})={conf_avg_cost}")

                    if normalize_results and not run_params['run_type'] == 'rewards':
                        try:
                            # normalizing average cost with (1-fn)
                            conf_avg_cost = conf_avg_cost / float(1-fn)
                            conf_avg_cost = 1 if conf_avg_cost > 1 else conf_avg_cost
                        except ZeroDivisionError:
                            # in case fn = 1 (100% fn) average cost should be 0
                            conf_avg_cost = 0
                        self.logger.info(
                            f"norm_conf_avg_cost-(fp={fp}, fn={fn}, attack={attack_id})={conf_avg_cost}")

                    df_fp_fn_cost[fp][fn] = conf_avg_cost

                # to manage all processes
                fp_fn_shared_dict[attack_id] = df_fp_fn_cost

                if create_fp_fn_results:
                    save_results(
                        {f"fp-fn/cost_fp_fn_{base_file_name}_{attack_id}": df_fp_fn_cost}, save_log=False)

                if create_attacks_results:
                    r = pd.DataFrame.from_dict(
                        {"attack": [attack_id],
                            "reward": [conf_avg_cost],
                            "num_iocs": [len(host_env.elements[attack_id])]})
                    attacks_df = attacks_df.append(r, ignore_index=True)

            if create_attacks_results:
                attacks_shared_dict[nproc] = attacks_df
                file_name = f"attacks_{base_file_name}"
                save_results({f"attacks/{file_name}": attacks_df})

        return

    def run_order_parallely(self, order, order_params,
                            config, nproc, debugrun):
        attacks = self.get_run_attacks(config)

        if len(attacks) < nproc:
            self.logger.error("Number of attacks is lower then nproc")
            exit()

        attacks_lst = list(zip(attacks.keys(), attacks.values()))
        attacks_chunks = split(attacks_lst, nproc)
        chunk_run_params = deepcopy(config['run_params'])

        m = Manager()
        attacks_shared_dict = m.dict()
        fp_fn_shared_dict = m.dict()
        procs = []

        if not debugrun:
            for idx, chunk in enumerate(attacks_chunks):
                chunk_run_params = deepcopy(config['run_params'])
                chunk_run_params['nproc'] = idx

                p = Process(target=self.run_order,
                            args=(order, order_params, chunk_run_params,
                                  chunk, attacks_shared_dict, fp_fn_shared_dict))
                procs.append(p)

            for idx, p in enumerate(procs):
                self.logger.info(f"Starting process {idx}")
                p.start()

            for idx, p in enumerate(procs):
                self.logger.info(f"Joining process {idx}")
                p.join()
        else:
            self.run_order(order, order_params, chunk_run_params,
                           attacks_lst, attacks_shared_dict, fp_fn_shared_dict)

        run_type = chunk_run_params["run_type"]
        create_fp_fn_results = chunk_run_params["create_fp_fn_results"]
        create_attacks_results = chunk_run_params["create_attacks_results"]
        base_file_name = f"{order.__name__}_{run_type}"

        if create_attacks_results:
            # concatenate all attacks to have single list with all attacks.
            merged_attacks = pd.concat(attacks_shared_dict.values())
            save_results({f"attacks_merged_{base_file_name}": merged_attacks})
        if create_fp_fn_results:
            # mean all fp-fn matrix to a single matrix
            merged_fp_fn = pd.concat(fp_fn_shared_dict.values()).groupby(
                level=0).mean(numeric_only=False)
            save_results({f"fp_fn_merged_{base_file_name}": merged_fp_fn})

    def get_run_attacks(self, config):
        run_params = config["run_params"]
        config_attacks = config['attacks']
        # in case none specific attacks are given through config
        # use all attacks
        if len(config_attacks) > 0:
            attacks = config_attacks
        else:
            self.logger.info("Using all attacks in dataset!")
            attacks = {a: str(a) for a in self.nutral_env.elements.keys()}
        niocs_upper_bound = run_params['niocs_upper_bound']
        niocs_lower_bound = run_params['niocs_lower_bound']
        # filter selected attacks by their size
        filtered_attacks = {k: v for (k, v) in attacks.items() if
                            (len(self.nutral_env.elements[int(k)]) >= niocs_lower_bound and
                             len(self.nutral_env.elements[int(k)]) <= niocs_upper_bound)}
        return filtered_attacks

    def generate_evading_attacks(self):
        # number of attacks proporionate to the precentage
        # self.env_builder.generate_evading_attack(
        #     self.nutral_env, 20, 500, 2000, 0.35, 111112)
        # self.env_builder.generate_evading_attack(
        #     self.nutral_env, 25, 500, 2000, 0.3, 111113) # best

        # self.env_builder.generate_evading_attack(
        #     self.nutral_env, 35, 500, 2000, 0.2, 111115)

        # self.env_builder.generate_evading_attack(
        #     self.nutral_env, 15, 2000, 4500, 0.16, 111119)
        # self.env_builder.generate_evading_attack(
        #     self.nutral_env, 20, 2000, 4500, 0.14, 111120)
        # self.env_builder.generate_evading_attack(
        #     self.nutral_env, 25, 2000, 4500, 0.12, 111121)
        # self.env_builder.generate_evading_attack(
        #     self.nutral_env, 30, 2000, 4500, 0.1, 111122)
        # self.env_builder.generate_evading_attack(
        #     self.nutral_env, 35, 2000, 4500, 0.08, 111123)

        # self.env_builder.generate_evading_attack(
        # self.nutral_env, 45, 300, 1000, 0.3, 111124)
        # self.env_builder.generate_evading_attack(
        #     self.nutral_env, 55, 100, 800, 0.3, 111125) # best

        # self.env_builder.generate_evading_attack(
        #     self.nutral_env, 45, 100, 800, 0.4, 111126)
        # self.env_builder.generate_evading_attack(
        #     self.nutral_env, 55, 100, 800, 0.3, 111127)
        # self.env_builder.generate_evading_attack(
        #     self.nutral_env, 60, 100, 600, 0.3, 111128)

        # self.env_builder.generate_evading_attack(
        # self.nutral_env, 65, 100, 800, 0.3, 111129) # best
        # self.env_builder.generate_evading_attack(
        # self.nutral_env, 75, 100, 800, 0.3, 111130) # best
        # self.env_builder.generate_evading_attack(
        # self.nutral_env, 80, 100, 1000, 0.3, 111131)
        self.env_builder.generate_evading_attack(
            self.nutral_env, 75, 100, 800, 0.6, 111132)  # best


def save_results(dfs_dict={}, save_log=False):
    """
    Save csv and log results file to the results dir
    """
    now = datetime.now()
    new_dir_name = f"{now.year}_{now.month}_{now.day}"
    full_results_dir_path = os.path.join(RESULTS_DIR, new_dir_name)

    jobid = environ.get('SLURM_JOBID')
    if jobid is not None:
        full_results_dir_path = os.path.join(full_results_dir_path, jobid)

    pathlib.Path(full_results_dir_path).mkdir(parents=True, exist_ok=True)
    pathlib.Path(os.path.join(full_results_dir_path, "attacks")
                 ).mkdir(parents=True, exist_ok=True)
    pathlib.Path(os.path.join(full_results_dir_path, "place")
                 ).mkdir(parents=True, exist_ok=True)
    pathlib.Path(os.path.join(full_results_dir_path, "fp-fn")
                 ).mkdir(parents=True, exist_ok=True)
    pathlib.Path(os.path.join(full_results_dir_path, "budget")
                 ).mkdir(parents=True, exist_ok=True)

    # save given dataframes with given names
    for file_name, df in dfs_dict.items():
        file_name = f"{file_name}_{now.hour}_{now.minute}.csv"
        full_path = os.path.join(full_results_dir_path, file_name)
        df.to_csv(full_path)

    # in case needed can save run log file
    if save_log:
        new_logger_file_name = f"runner_{now.hour}_{now.minute}.log"
        shutil.copy("runner.log", os.path.join(
            full_results_dir_path, new_logger_file_name))


def create_logger(verbose, logger_file_name="runner.log"):
    console_level = logging.DEBUG if verbose else logging.INFO
    logger = logging.getLogger(logger_file_name.split('.')[0])
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler(logger_file_name, mode='w')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(console_level)
    # create formatter and add it to the handlers
    formatter = logging.Formatter(
        '%(asctime)s-%(name)s-%(levelname)s-%(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)
    return logger


def split(a, n):
    k, m = divmod(len(a), n)
    return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))


def load_json(path, logger):
    with open(path, 'r') as j:
        json_data = json.load(j)
        logger.info(f"Loading configuration file {path}")
    return json_data


def print_nice(logger, name, o):
    # log current configuration nicely
    logger.info("=" * 15 + " " + name + " " + "=" * 15)
    for line in pformat(o).split('\n'):
        logger.info(line)
    logger.info("=" * 42)
