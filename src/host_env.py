import os
import pickle
import random
import itertools

import numpy as np
import pandas as pd

from os.path import exists
from itertools import chain
from collections import namedtuple

from data_builder import NeoDataBuilder
# every instance in the host_env
IOC = namedtuple("IOC", "id type name cost occurs removed")

COST_RANGE = range(1, 2)
IOCS_DESCRIPTION = ["t:file", "t:ipv4_addr",
                    "t:domain_name", "t:url",
                    "t:process", "t:email",
                    "t:windows_registry_keys", "t:mutex"]

###########
# QUERIES #
###########

# all weak observables of attack (a.k.a as telemetries)
# the {iocs} in the template is validating the type of ioc (url, ip ...)
q_iocs_of_attack = """
    match (m:malware)<--(:indicator)<--(:observed_data)-[*1..3]-(t)
    where id(m) = $malware_id and ({iocs})
    RETURN distinct id(t) as id, t.name as name, t.type as type
"""
# all attacks
q_all_attacks = """
    match (:indicator)-->(m:malware)
    return distinct id(m) as id
"""


class HostEnvironment(object):
    """Represents the host environment simulation.

    By simulation we means that it represent a host in which certain IOCs
    are occuring and some are not. When such HostEnvironment object is iniallized
    it is "infected" with certain attack. The "infection" can be seen is reflected
    by the IOC with IOC.occurs = True.

    Attributes
    ----------
    elements:
        Dict, The key is `id` of certain attack and the value is
              list of IOC objects which describe all the IOCs of the attack
              described by `id`.

    attack_iocs:
        List(), ids of all occuring iocs of occuring attack in the simulation.

    _shared_iocs:
        Dict, dict of IOC id pointing to a list of attacks sharing this IOC.
    """

    def __init__(self, elements, logger=None):

        self.logger = logger
        self.elements = elements

        self.occuring_attack_id = None
        self.occuring_attack_iocs = None

        self._shared_iocs = None
        # dict in which key=ioc id, value=occurs (bool)
        self._iocs_occuring_map = {}
        # dict with all iocs that already been searched
        # key=ioc.id, value=ioc
        self._iocs_search_db = {}
        # whether the occuring attack is removed from env
        self ._remove_occuring_attack = False

    def remove_ioc_from_env(self, ioc_id):
        attacks_removed_from = []
        if self._shared_iocs:
            attacks_share_ioc = self._shared_iocs[ioc_id]
            for attack_sharing_id in attacks_share_ioc:
                self.remove_ioc_from_attack(ioc_id, attack_sharing_id)
            attacks_removed_from = attacks_share_ioc
        else:
            for attack_id in self.elements.keys():
                removed = self.remove_ioc_from_attack(ioc_id, attack_id)
                if removed:
                    attacks_removed_from.append(attack_id)

        return attacks_removed_from

    def remove_ioc_from_attack(self, ioc_id, attack_id):
        if attack_id == self.occuring_attack_id:
            iocs = self.occuring_attack_iocs
        else:
            # in case occuring attack is not in env
            if self._remove_occuring_attack:
                # update occuring attack iocs as well
                self._remove_ioc_from_list(ioc_id, self.occuring_attack_iocs)

            iocs = self.elements[attack_id]

        return self._remove_ioc_from_list(ioc_id, iocs)

    def _remove_ioc_from_list(self, ioc_id, lst, occurs=None):
        for idx, ioc in enumerate(lst):
            # if found desried ioc
            if ioc.id == ioc_id and not ioc.removed:
                if occurs is not None:
                    occurs_to_set = occurs
                else:
                    occurs_to_set = ioc.occurs

                lst[idx] = IOC(ioc.id, ioc.type, ioc.name,
                               ioc.cost, occurs_to_set, True)
                return True
        return False

    def remove_attack_from_env(self, attack_id):
        # remove from elements itself the attack
        iocs_to_pop = []
        self.elements.pop(attack_id)
        # remove from _shared_iocs
        if self._shared_iocs:
            for ioc_id, sharing_attacks in self._shared_iocs.items():
                if attack_id in sharing_attacks:
                    sharing_attacks.remove(attack_id)
                # remove ioc in case it was shared only
                # by given attack
                if not sharing_attacks:
                    iocs_to_pop.append(ioc_id)
            for ioc in iocs_to_pop:
                self._shared_iocs.pop(ioc)
                # IOCs that are not shared with other attacks should be marked
                # as removed.
                self._remove_ioc_from_list(
                    ioc, self.occuring_attack_iocs, occurs=False)
        else:
            self.logger.warning(
                "Didn't remove attack from shared ioc (not initiallized)")

    def get_iocs_left(self, attack_id):
        """
        Return all iocs related to given attack id in which
        removed = False.
        """
        iocs = self.occuring_attack_iocs if attack_id == self.occuring_attack_id else self.elements[
            attack_id]
        return list(filter(lambda ioc: not ioc.removed, iocs))

    def get_num_occuring_iocs_left(self, attack_id):
        """
        Return number of iocs related to given attack id in which
        removed = False.
        """
        return len(self.get_occuring_iocs_left(attack_id))

    def get_occuring_iocs_left(self, attack_id):
        """
        Return all iocs related to given attack id in which
        removed = False.
        """
        if attack_id:
            iocs = self.occuring_attack_iocs if attack_id == self.occuring_attack_id else self.elements[
                attack_id]
        # no specific attack, look on all env
        if not attack_id:
            # we're using set in order to search every ioc once.
            iocs = list(set(itertools.chain(*self.elements.values())))

        return list(filter(lambda ioc: not ioc.removed and ioc.occurs, iocs))

    def get_num_iocs_left(self, attack_id):
        """
        Return number of iocs related to given attack id in which
        removed = False.
        """
        return len(self.get_iocs_left(attack_id))

    def get_iocs_discoverd(self, attack_id):
        if attack_id:
            iocs = self.occuring_attack_iocs if attack_id == self.occuring_attack_id else self.elements[
                attack_id]
        # no specific attack, look on all env
        if not attack_id:
            # we're using set in order to search every ioc once.
            iocs = list(set(itertools.chain(*self.elements.values())))

        return list(filter(lambda ioc: ioc.removed and ioc.occurs, iocs))

    def get_num_iocs_discoverd(self, attack_id):
        return len(self.get_iocs_discoverd(attack_id))

    def get_iocs_exits_and_left(self, attack_id):
        left_iocs = self.get_iocs_left(attack_id)
        # for every ioc, check if still exists in the knowledge base.
        return list(filter(lambda ioc: self._shared_iocs.get(ioc.id), left_iocs))

    def get_random_max_shared_ioc(self, attack_id):
        """
        Get the ioc from the given iocs_list which is shared among the
        biggest number of attacks.Tie braker is random.
        """
        # get number of sharing attack for each ioc
        ioc_num_shared = []
        attack_iocs = self.get_iocs_left(attack_id)

        # no relevant iocs left
        if not attack_iocs:
            return None

        for ioc in attack_iocs:
            if not ioc.removed:
                ioc_num_shared.append(len(self._shared_iocs[ioc.id]))
            else:
                # so it won't be selected
                ioc_num_shared.append(-1)

        max_indices = multiple_max(ioc_num_shared)
        # random tie braker
        random_max = random.choice(max_indices)
        return attack_iocs[random_max]

    def calculate_shared_iocs(self):
        """
        return
        ------
            dict of IOC id pointing to a list of attacks sharing this IOC.
        """

        # check if already calculated - for example when loading pickle
        if self._shared_iocs:
            if not hasattr(self, "attacks_sharing_precentage"):
                self._calculate_sharing_precentage()
            self.logger.debug("Shared iocs already calculated")
            return

        self.logger.debug("Calculating shared iocs")
        shared_iocs_dict = {}
        concatenated_iocs = self.elements.values()

        all_unique_iocs = set(list(chain(*concatenated_iocs)))
        all_unique_iocs = [i.id for i in all_unique_iocs]

        for ioc in all_unique_iocs:
            shared_attacks = []
            for attack, iocs_list in self.elements.items():
                if ioc in [i.id for i in iocs_list]:
                    shared_attacks.append(attack)
            shared_iocs_dict[ioc] = shared_attacks
        self.logger.debug("Finished calculating shared iocs")
        self._shared_iocs = shared_iocs_dict
        self._calculate_sharing_precentage()

    def _calculate_sharing_precentage(self):
        """Calculate the precentage of iocs shared with other attacks"""
        self.logger.info("Caculating attacks sharing precentage")
        attacks_sharing_precentage = {}
        for attack, iocs in self.elements.items():
            num_sharing = 0
            for ioc in iocs:
                if len(self._shared_iocs[ioc.id]) > 1:
                    num_sharing += 1
            attacks_sharing_precentage[attack] = num_sharing/float(len(iocs))
        self.attacks_sharing_precentage = attacks_sharing_precentage

    def _get_search_result(self, ioc):
        res = self._iocs_search_db.get(ioc.id)
        if not res:
            self._iocs_search_db[ioc.id] = ioc
        return res


def multiple_max(x):
    mx = max(x)
    m = [i for i, j in enumerate(x) if j == mx]
    return m


class HostEnvironmentBuilder(object):
    def __init__(self, neo_builder_creds, logger):
        self._neo_builder = NeoDataBuilder(neo_builder_creds, logger)
        self.logger = logger

    def get_iocs_by_attacks(self):
        """Get dict of attacks pointing to their iocs.
        """
        d_iocs_by_attack = {}
        all_attacks = self._neo_builder.get_generic_query(
            q_all_attacks, {}, "id")

        for attack_id in all_attacks:
            attack_iocs = self.get_iocs_of_attack(attack_id)
            d_iocs_by_attack[attack_id] = attack_iocs

        return d_iocs_by_attack

    def get_iocs_of_attack(self, attack_id, iocs_description=IOCS_DESCRIPTION):
        """Get all iocs of given attack from the Neo4j grpah db.

        input
        -----

        return
        ------
            [attack_iocs]
        """
        query_with_iocs_description = q_iocs_of_attack.format(
            iocs=" or ".join(iocs_description))
        attack_iocs = self._neo_builder.get_generic_query(
            query_with_iocs_description, {"malware_id": attack_id})
        return attack_iocs

    def build_nutral_env(self, run_params):
        """Build host env environment list.
        The host env is a list of IOC namedtupple or a dict of attacks pointing
        to lists of of IOC namedtuple.
        Each IOC represents ioc on the host environment and it's state.

        input
        -----
            :param load_pickle_env (Boolean): indicating if should build
                    nutral env from pickle file if exists.

        return
        ------
            host env
        """
        host_env_file = run_params.get("host_env_file")
        if host_env_file:
            host_env_file_path = os.path.join(os.getenv("BASE_DIR"),"experiments", 'envs', host_env_file)
            self.logger.info(
                f"Using prebuilt pickle host env ({host_env_file_path})")
            with open(host_env_file_path, "rb") as pickled_env:
                return pickle.loads(pickled_env.read())
        elif not self._neo_builder.driver:
            raise Exception("Neo4j server is not available.")

        host_env = []
        self.logger.debug("Building nutral host environment")
        d_iocs_by_attack = self.get_iocs_by_attacks()
        for attack, iocs_df in d_iocs_by_attack.items():
            d_iocs_by_attack[attack] = HostEnvironmentBuilder.ids_to_IOCs(
                iocs_df)
        host_env = d_iocs_by_attack
        self.logger.debug("Finished building host environment")
        return HostEnvironment(host_env, logger=self.logger)

    @ staticmethod
    def ids_to_IOCs(iocs_df):
        """Transform the query result into namedtuples representing IOC.

        input
        -----
            :param iocs_df (pandas.DataFrame): data frame containing all the data
                           describing the iocs.
        return
        ------
            host env

        """
        env = []
        for _, row in iocs_df.iterrows():
            # random cost from cost range
            cost = np.random.choice(COST_RANGE)
            env.append(
                IOC(id=row['id'], type=row['type'],
                    name=row['name'], cost=cost, occurs=None, removed=False))
        return env

    def set_env(self, host_env, occuring_attack_id, fp=0.0, fn=0.0, remove_attack=False):
        """Configure given env (usually nutral) with given params.

        HostEnviroment is created nutral (without any occuring attack) and
        the occuring attack is configured per ran.

        input
        -----
            :param host_env (HostEnviroment): object describing the host
                    env, usually will be nutral env.
            :param occuring_attack_id (int): id of occuring attack.
            :param fp (0 <= float <= 1): precnetage of false positive. This
                    will influence on the 'occurs' member in the occuring attack iocs.
            :param fn (0 <= float <= 1): precnetage of false negative. This
                    will influence on the 'occurs' member in the not occuring attacks
                    iocs.
            :param remove_attack (bool): indicating whether to remove occuring attack from env.

        return
        ------
            None, configuring host env *in place*.
        """
        host_env.occuring_attack_id = occuring_attack_id
        # get occuring attack iocs from host enivronment
        host_env.occuring_attack_iocs = host_env.elements[occuring_attack_id]
        occuring_attack_iocs_ids = [
            ioc.id for ioc in host_env.occuring_attack_iocs]
        host_env._remove_occuring_attack = remove_attack
        for _, iocs in host_env.elements.items():
            for idx, ioc in enumerate(iocs):
                if ioc.id in occuring_attack_iocs_ids:
                    # [False, True]
                    probs = [fn, 1-fn]
                else:
                    # [False, True]
                    probs = [1-fp, fp]

                # in case this is a shared ioc which already
                # have a value generated by probs
                val = host_env._iocs_occuring_map.get(ioc.id)
                if val is None:
                    val = np.random.choice([False, True], size=1, p=probs)[0]
                    host_env._iocs_occuring_map[ioc.id] = val

                iocs[idx] = IOC(
                    ioc.id, ioc.type, ioc.name, ioc.cost, val, ioc.removed)

        if remove_attack:
            # remove from elements and shared iocs.
            host_env.remove_attack_from_env(occuring_attack_id)

    def generate_evading_attack(self, nutral_host_env, num_sharing,
                                min_size, max_size, prec, evading_attack_id=1111111):
        """Generating a new 'evading' attack in the given host environment.

        input
        -----
            :param nutral_host_env (HostEnviroment): object describing the host
                    env in which the attack will be build upon and added to.
            :param fp (0 <= float <= 1): precnetage of false positive. This
                    will influence on the 'occurs' member in the occuring attack iocs.
            :param fn (0 <= float <= 1): precnetage of false negative. This
                    will influence on the 'occurs' member in the not occuring attacks
                    iocs.
            :param remove_attack (bool): indicating whether to remove occuring attack from env.
            :param attack_id (int): id of the new generated attack.

        return
        ------
            None, configuring host env *in place*.
        """

        # get all attacks with matching size
        attacks_with_size = []
        for attack_id, iocs in nutral_host_env.elements.items():
            if len(iocs) >= min_size and len(iocs) <= max_size:
                attacks_with_size.append(attack_id)

        # next we select which attack to use from the one with matching size
        selected_attacks = random.sample(attacks_with_size, num_sharing)

        # next we select iocs from each of the attacks
        evading_attack_iocs = []
        for attack_id in selected_attacks:
            attack_iocs = nutral_host_env.elements[attack_id]
            amount_to_draw = int(len(attack_iocs) *
                                 prec) if type(prec) is float else prec
            shared_iocs = random.sample(attack_iocs, amount_to_draw)
            evading_attack_iocs += shared_iocs
        nutral_host_env.elements[evading_attack_id] = evading_attack_iocs
        self.logger.info(
            f"Generated evading attack with {len(evading_attack_iocs)} iocs")

        # update _shared_iocs
        for evading_ioc in evading_attack_iocs:
            nutral_host_env._shared_iocs[evading_ioc.id].append(
                evading_attack_id)
